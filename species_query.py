#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Example query to retrieve entries based on a specific species.

Author: Rik van Rosmalen (rikpetervanrosmalen@gmail.com)
"""
import sqlite3
import pathlib

import pandas as pd


def get_protein_referenced_tables(conn):
    table_query = """
    SELECT name
    FROM sqlite_master
    WHERE type='table'
    AND instr(name, "_prot") > 0
    """
    for entry, *_ in conn.execute(table_query):
        yield entry[:-5]


def get_species_entries(conn, species, category):
    template = """
    SELECT
        protein.organism, protein.uniprot, protein.swissprot,
        {table}_prot.prot_id,
        {table}.*
    FROM {table}_prot
    INNER JOIN protein
    ON (
        protein.ec = {table}_prot.ec
        AND
        protein.id = {table}_prot.prot_id
    )
    INNER JOIN {table}
    ON (
        {table}_prot.entry = {table}.entry
        AND
        {table}_prot.ec = {table}.ec_number
    )
    WHERE instr(protein.organism, "{species}") > 0
    ORDER BY {table}.ec_number, {table}.entry
    """
    return pd.read_sql_query(template.format(table=category, species=species), conn)


def get_species_literature(conn, species, category):
    template = """
    SELECT DISTINCT
        "{table}" AS origin,
        {table}_ref.entry,
        literature.*
    FROM {table}_prot
    INNER JOIN protein
    ON (
        protein.ec = {table}_prot.ec
        AND
        protein.id = {table}_prot.prot_id
    )
    INNER JOIN {table}_ref
    ON (
        {table}_prot.entry = {table}_ref.entry
        AND
        {table}_prot.ec = {table}_ref.ec
    )
    INNER JOIN literature
    ON (
        {table}_ref.ref_id = literature.id
        AND
        {table}_ref.ec = literature.ec
    )
    WHERE instr(protein.organism, "{species}") > 0
    """
    return pd.read_sql_query(template.format(table=category, species=species), conn)


if __name__ == "__main__":
    database_path = pathlib.Path("brenda.db").resolve()
    output_path = pathlib.Path("brenda_species_query.xlsx").resolve()
    species = "Pseudomonas putida"

    assert database_path.exists()
    with sqlite3.connect(database_path) as conn, pd.ExcelWriter(output_path) as writer:
        literature = []
        # Get entries per table
        for table in get_protein_referenced_tables(conn):
            get_species_entries(conn, species, table).to_excel(
                writer, sheet_name=table, engine="xlsxwriter"
            )
            literature.append(get_species_literature(conn, species, table))

        # Also get related literature entries
        pd.concat(literature).sort_values(["origin", "ec", "entry"]).to_excel(
            writer, sheet_name="literature", engine="xlsxwriter"
        )
