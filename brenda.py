#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Script to parse the BRENDA flat file download.

Author: Rik van Rosmalen (rikpetervanrosmalen@gmail.com)

See also README.txt that can be found for download on the BRENDA website.

Information about the file structure:

Each block:
    - starts with ID{tab}{e.c.}
    - copyright notice
    - information fields:
        - start with full name
        - short name{tab}{entry}
        - ends with 1+ newlines
    - ends with '///'.

References within entries:
    - #...# indicates a protein reference
    - <...> indicates a literature reference
    - (...) indicates a comment
    - {...} indicates field specific information.

Note that:
    - Compounds names can contain:
        - (multiple nested) {[()]}
        - Spaces (usually nested inside parentheses)
        - arrows '->' (complex sugars) or '='
    - Reaction entries might have a reaction formula:
        - One or more entries separated by a '+'
        - Sides are separated by '='
            A few entries are missing an '=' or have an extra '=' at the end
        - Stoichiometry is optional (Presumably defaults to 1)
        - Unknown reaction mechanisms use a '?'
        - Some reactions are descriped by their mechanism instead
    - Comments can also have metabolite names and reactions inside and all
        associated characters.
    - Numerical values are denoted as:
        - single value
        - a range of values separated by a '-'
    - Not all entries seem to be unique.
    - Additional (low quality data) values can be saved under a 'more'
        or 'more = ?' entry. These entries will have -999 as value
        if required by the category.

List of short entry name:
    AC   - activating compound
    AP   - application
    CF   - cofactor
    CL   - cloned
    CR   - crystallization
    EN   - engineering
    EXP  - expression
    GI   - general information on enzyme
    GS   - general stability
    IC50 - IC-50 Value substrate in {...} (Not mentioned in README)
    ID   - EC-class (Only for headers)
    IN   - inhibitors
    KI   - Ki-value    inhibitor in {...}
    KKM  - Kcat/KM-Value substrate in {...} (Substrate is missing)
    KM   - KM-value    substrate in {...}
    LO   - localization
    ME   - metals/ions
    MW   - molecular weight
    NSP  - natural substrates/products reversibility information in {...}
    OS   - oxygen stability
    OSS  - organic solvent stability
    PHO  - pH-optimum
    PHR  - pH-range
    PHS  - pH stability
    PI   - isoelectric point
    PM   - posttranslation modification
    PR   - protein
    PU   - purification
    RE   - reaction catalyzed
    REN  - renatured
    RF   - references  pubmed in {...} (Not mentioned in README)
    RN   - accepted name (IUPAC)
    RT   - reaction type
    SA   - specific activity
    SN   - synonyms
    SP   - substrates/products reversibility information in {...}
    SS   - storage stability
    ST   - source/tissue
    SU   - subunits
    SY   - systematic name
    TN   - turnover number substrate in {...}
    TO   - temperature optimum
    TR   - temperature range
    TS   - temperature stability
"""
import re
import warnings
import itertools
import collections
import json
import sqlite3
import os

# Entry headers
ENTRY_HEADERS = {
    "AC": "ACTIVATING_COMPOUND",
    "AP": "APPLICATION",
    "CF": "COFACTOR",
    "CL": "CLONED",
    "CR": "CRYSTALLIZATION",
    "EN": "ENGINEERING",
    "EXP": "EXPRESSION",
    "GI": "GENERAL_INFORMATION",
    "GS": "GENERAL_STABILITY",
    "IC50": "IC50_VALUE",
    # "ID": "ID", # Already matched for the EC section start
    "IN": "INHIBITORS",
    "KI": "KI_VALUE",
    "KKM": "KCAT_KM_VALUE",
    "KM": "KM_VALUE",
    "LO": "LOCALIZATION",
    "ME": "METALS_IONS",
    "MW": "MOLECULAR_WEIGHT",
    "NSP": "NATURAL_SUBSTRATE_PRODUCT",
    "OS": "OXIDATION_STABILITY",
    "OSS": "ORGANIC_SOLVENT_STABILITY",
    "PHO": "PH_OPTIMUM",
    "PHR": "PH_RANGE",
    "PHS": "PH_STABILITY",
    "PI": "PI_VALUE",
    "PM": "POSTTRANSLATIONAL_MODIFICATION",
    "PR": "PROTEIN",
    "PU": "PURIFICATION",
    "RE": "REACTION",
    "REN": "RENATURED",
    "RF": "REFERENCE",
    "RN": "RECOMMENDED_NAME",
    "RT": "REACTION_TYPE",
    "SA": "SPECIFIC_ACTIVITY",
    "SN": "SYSTEMATIC_NAME",
    "SP": "SUBSTRATE_PRODUCT",
    "SS": "STORAGE_STABILITY",
    "ST": "SOURCE_TISSUE",
    "SU": "SUBUNITS",
    "SY": "SYNONYMS",
    "TN": "TURNOVER_NUMBER",
    "TO": "TEMPERATURE_OPTIMUM",
    "TR": "TEMPERATURE_RANGE",
    "TS": "TEMPERATURE_STABILITY",
}

SHORT_NAME_REGEX = re.compile(
    "|".join("(?:^({})\t)".format(i) for i in sorted(ENTRY_HEADERS.keys()))
)
FULL_NAME_REGEX = re.compile(
    "|".join("(?:^({})$)".format(i) for i in sorted(ENTRY_HEADERS.values()))
)

PARSE_SPECIFIC_FIELD = {"IC50", "KKM", "KI", "KM", "NSP", "SP", "TN", "RF"}
PARSE_COMMENT_WTH_PROT_ONLY = {"AC", "IN", "CF", "RE", "SP", "NSP"}
SPECIFIC_FIELD_AFTER_COMMENT = {"SP", "NSP"}
EXTRA_COMMENT_WITH_VBARS = {"SP", "NSP"}

# Brenda references
PROT_REF_REGEX = re.compile(r"#([\d,\s]+)#")
LIT_REF_REGEX = re.compile(r"<([\d,\s]+)>")
COMMENT_STRICT_REF_REGEX = re.compile(r"(?:\A|\s)\((.+)\)(?:\s|\Z)", re.DOTALL)
COMMENT_STRICT_WITH_PROT_REF_REGEX = re.compile(
    r"(?:\A|\s)\((#.+)\)(?:\s|\Z)", re.DOTALL
)
COMMENT_STRICT_WITH_PROT_LIT_REF_AND_VBARS_REGEX = re.compile(
    r"(?:\A|\s)\((#.+>)\)(?:\s|\Z)\s?(?:\|(.*)\|)?", re.DOTALL
)
# Order agnostic version.
# (?:(?:(?:\A|\s)(?:\((#.+>)\)(?:\s|\Z)))|(?:\|(.*)\|)){1,2}
SPECIFIC_FIELD_STRICT_REGEX = re.compile(r"(?:\A|\s)\{(.*)\}(?:\s|\Z)", re.DOTALL)
REACTION_EXTRA_COMMENT_REGEX = re.compile(r"\|(.*?)\|")

# Parser regex
STARTING_PROT_REF_REGEX = re.compile(r"^#[,\d\s]+#")
ENDING_LIT_REGEX = re.compile(r"<[,\d\s]+>$")
PH_REGEX = re.compile(r"pH\s(\d+\.?\d*)")
TEMP_REGEX = re.compile(r"(-?\d+\.?\d*)°C")
# EC regex taken from identifiers.org
EC_REGEX = re.compile(
    r"\d+\.-\.-\.-|\d+\.\d+\.-\.-|\d+\.\d+\.\d+\.-|\d+\.\d+\.\d+\.(n)?\d+"
)
PUBMED_REGEX = re.compile(r"Pubmed:(\d+)")
UNIPROT_REGEX = re.compile(r"(\w+)\sUniProt")
SWISSPROT_REGEX = re.compile(r"(\w+)\sSwissProt")
# Uniprot regex taken from uniprot website.
SECONDARY_UNIPROT_REGEX = re.compile(
    r"[OPQ][0-9][A-Z0-9]{3}[0-9]|[A-NR-Z][0-9]" r"([A-Z][A-Z0-9]{2}[0-9]){1,2}"
)
STOICHIOMETRY_REGEX = re.compile(r"(^\d*\s)?(.+)")
WILD_TYPE_REGEX = re.compile(r"wild[\s-]type", re.IGNORECASE)
MUTANT_REGEX = re.compile(r"mutant|mutation|chimera", re.IGNORECASE)
RANGE_REGEX = re.compile(r"(-?\d+\.?\d*)-(-?\d+\.?\d*)")
# For the below regex, note the re.Verbose means that each space needs to be written
# explicitly via "\s".
REACTION_DESCRIPTION_REGEX = re.compile(
    r"""
    (^(
        (random|preferential)?\s?(exo|endo)?-?hydrolys[ei]s
        |release
        |(auto)?(catalytically|catalyses)
        |th(e|is)\s(recombinant\s)?enzyme
        |the\s?(primary|secondary)?\scleavage
        |(hydrolytic|two-stage|eliminative|preferential
          |selective|(endo|exo)(nucleo)?(type|lytic)|[CN]-terminal
          |[35]'-end|directed|\s
          )*
          \s?cleavage
        |(fully)?\s?(in)?activates
        |acts\son
        |strict\srequirement\sfor
        |(similar(,\sbut\snot\sidentical,)?|corresponding)\sto
        |Epimerization
        |ATP-(in)?dependent\sbreakage
    )\b)
    |((\sis|contains|cleaves|hydrolyses|digests|catalyses|(in)?activates
       |converts|breaks|transfers|adds|eliminates|differs|cyclizes|removes
       |releases|processes|produces|exhibits
       )\b)
    |((digestion|transfer|deacetylation|release|cleavage|degradation
      |removal|endopeptidase|elimination|hydrolysis|interconversion)
      \sof\b)
    |((\A|\s)
      (activity|specificity|known\sto\sbe|requirement\sfor
       |bonds\scleaved|(endo)?peptidase\saction)
      \b)
    """,
    flags=re.IGNORECASE | re.VERBOSE,
)


def nested_split(matches, sep=re.compile(r"[\s,]")):
    return list(itertools.chain.from_iterable(re.split(sep, i) for i in matches))


def scan(
    text, end0, end1, closers="]})>", openers="[{(<", not_nestable="|#", skiparrows=True
):
    """Scan text, returning the position of a character in end0 followed by a
    character in end1, but only when not surrounded by any grouping character.
    """
    openers = {j: i for i, j in enumerate(openers)}
    closers = {j: i for i, j in enumerate(closers)}
    not_nestable = {j: i for i, j in enumerate(not_nestable, start=len(openers))}
    # Create a stack to keep track of the nested level.
    stack = [0] * (len(openers) + len(not_nestable))
    for idx, c in enumerate(text):
        if (
            skiparrows
            and c in "<>"
            and len(text) > (idx + 1)
            and idx > 0
            and ("-" in (text[idx - 1], text[idx + 1]))
        ):
            continue
        elif c in openers:
            stack[openers[c]] += 1
        elif c in closers:
            stack[closers[c]] -= 1
        elif c in not_nestable:
            # We just assume here that every 1st one opens and every 2nd one closes
            # since you cannot differentiate anyway.
            stack[not_nestable[c]] = (stack[not_nestable[c]] + 1) % 2
        # Use absolute for reverse scan where we see the closer first.
        if (
            sum(abs(i) for i in stack) == 0
            and c in end0
            and (idx + 1 == len(text) or text[idx + 1] in end1)
        ):
            return idx
    return idx


def parse_reaction_string_metabolites(reaction_text):
    try:
        reactants, products = (
            [m.strip() for m in i.split(" + ") if m.strip()]
            for i in reaction_text.split("=")
        )
    except ValueError:
        raise ValueError("Could not parse metabolites: {}".format(reaction_text))
    reactants = [get_stoichiometry(m) for m in reactants]
    products = [get_stoichiometry(m) for m in products]
    return reactants, products


def get_stoichiometry(text):
    match = STOICHIOMETRY_REGEX.search(text)
    if match is None:
        raise ValueError("Could not parse metabolite stoichiometry: {}".format(text))
    n = 1 if match.group(1) is None else int(match.group(1).strip())
    return n, match.group(2)


class EmptyEntryException(ValueError):
    pass


class Parser:
    def __init__(self, debug=False):
        self.debug = debug

    def parse(self, entry, ec_number):
        try:
            short_name, entry = entry.split("\t", 1)
        except ValueError:
            raise EmptyEntryException
        prot = sorted(
            [int(i) for i in set(nested_split(PROT_REF_REGEX.findall(entry))) if i]
        )
        lit = sorted(
            [int(i) for i in set(nested_split(LIT_REF_REGEX.findall(entry))) if i]
        )

        # Not all categories have a specific field, but they might have curly braces in the
        # comment field, so better to skip if not required.
        if short_name in PARSE_SPECIFIC_FIELD:
            specific_field = SPECIFIC_FIELD_STRICT_REGEX.search(entry)
        else:
            specific_field = None

        # Start looking for comment only after specific fields are found, as
        # specific fields might have parentheses inside.
        start_comment = 0
        if specific_field:
            if short_name not in SPECIFIC_FIELD_AFTER_COMMENT:
                start_comment = specific_field.span(1)[-1] - 1

        # We start checking the comment only after the specific field
        # Unless we had one of the exceptions fields, in which case we have start_comment
        # already set to 0.
        if short_name in EXTRA_COMMENT_WITH_VBARS:
            comment = COMMENT_STRICT_WITH_PROT_LIT_REF_AND_VBARS_REGEX.search(
                entry[start_comment:]
            )
            if not comment:
                comment = REACTION_EXTRA_COMMENT_REGEX.search(entry[start_comment:])
        elif short_name in PARSE_COMMENT_WTH_PROT_ONLY:
            comment = COMMENT_STRICT_WITH_PROT_REF_REGEX.search(entry[start_comment:])
        else:
            comment = COMMENT_STRICT_REF_REGEX.search(entry[start_comment:])

        # Create a cleaned up version of full text.
        cleaned = entry
        if comment:
            cleaned = cleaned.replace(comment.group(0), "")
            if comment and len(comment.groups()) > 1:
                comment = "; ".join((group for group in comment.groups() if group))
            elif comment:
                comment = comment.group(1)
        if specific_field:
            cleaned = cleaned.replace("{{{}}}".format(specific_field.group(1)), "")
            specific_field = specific_field.group(1)
        cleaned = self.remove_start_protein_reference(cleaned)
        cleaned = self.remove_end_lit_reference(cleaned)
        cleaned = cleaned.strip()

        parsed_entry = {
            "ec_number": ec_number,
            "short_name": short_name,
            "full_text": entry,
            "cleaned": cleaned,
            "protein_ref": prot,
            "literature_ref": lit,
            "comment": comment,
            "specific_field": specific_field,
        }

        f_name = "_parse_{}".format(short_name)
        if f_name in dir(self):
            f = getattr(self, f_name)
            parsed_entry = f(parsed_entry)
        else:
            raise ValueError("No parsers for {}.".format(short_name))

        if not self.debug:
            # These should be fully processed, so can be removed to save space.
            del parsed_entry["full_text"]
            del parsed_entry["cleaned"]
            del parsed_entry["short_name"]
            del parsed_entry["specific_field"]
        return parsed_entry

    def remove_start_protein_reference(self, text):
        return STARTING_PROT_REF_REGEX.sub("", text)

    def remove_end_lit_reference(self, text):
        return ENDING_LIT_REGEX.sub("", text)

    def get_temp_ph(self, text):
        if text is None:
            return None, None
        temp = TEMP_REGEX.search(text)
        if temp:
            temp = float(temp.group(1))
        ph = PH_REGEX.search(text)
        if ph:
            ph = float(ph.group(1))
        return temp, ph

    def get_pubmed_id(self, text):
        if text is None:
            return None
        pubmed = PUBMED_REGEX.search(text)
        if pubmed:
            pubmed = pubmed.group(1)
        return pubmed

    def get_reversibility(self, text):
        if "{ir}" in text:
            return False
        elif "{r}" in text:
            return True
        return None

    def get_range(self, text):
        match = RANGE_REGEX.search(text)
        return float(match.group(1)), float(match.group(2))

    def get_single_or_range(self, text, entry, key):
        try:
            entry[key] = float(text)
            entry[key + "_min"] = None
            entry[key + "_max"] = None
        except ValueError:
            mi, ma = self.get_range(text)
            entry[key] = None
            entry[key + "_min"] = mi
            entry[key + "_max"] = ma
        return entry

    def parse_reaction_string(self, text):
        """Separate the reaction and the comment.

        In contrast to most other entries, parsing the reaction is rather more complicated, as
        metabolites can have both spaces and parenthesis in their name like the text in the comments.

        Furthermore, the comment itself can contain + and = as well.

        This makes it non-trivial to parse using regex, so we use a simple stack based parser.
        We start at the end, to discern the comment, stopping when we see a + or = that is not
        nested within any other operator.
        Then we do a forward scan from this point on to find the start of the actual comment.

        One thing it will not parse is the following examples:
        A + B = C + a (a)a (actual comment)
        A + B = C + a (a)a
        where a (a)a a is supposed to be one metabolite name...
        Also can't handle unpaired -> in the reaction name of some complex sugar complexes.
        """
        # First do a backward scan: Either to the first + or = of the reaction
        rev_text = text[::-1]
        idx = scan(rev_text, "+=", " ")
        # Don't forget to flip the index, since we have the reverse index!
        last_reaction_bit = text[-idx + 1 :]
        # Now do a forward scan to determine the end of the last metabolite.
        # Denoted by the start of a lit reference '<', comment '#', specific field '{' or protein '#'.
        # Additionally, this field has an extra comment field (?) wrapped in | ... |
        rxn_end = scan(last_reaction_bit, " ", "<({#|")

        # Recombine the reaction parts
        reaction = text[: -idx + 1] + last_reaction_bit[: rxn_end + 1]
        # All the rest is the comment.
        comment = last_reaction_bit[rxn_end + 1 :]
        return reaction.strip(), comment.strip()

    def parse_reaction(self, entry):
        # Clean comments, references etc. from entry full_text before we start to process.
        text = entry["cleaned"]

        # Check for common terms that are used to start a description of a reaction
        # instead of a formula.
        if re.search(REACTION_DESCRIPTION_REGEX, text) or text == "more = ?":
            entry["reaction_description"] = text
            entry["reaction_formula"] = None
            entry["reactants"] = None
            entry["products"] = None
        else:
            # This step is not really required any more with the improved regex.
            # reaction_text, other_text = self.parse_reaction_string(text)
            reaction_text = text
            # other_text = other_text.strip()
            # if other_text:
            #     warnings.warn(
            #         "Unrecognized fragment in entry ({}) reaction: {} |||{}|||".format(
            #             entry["ec_number"], reaction_text, other_text
            #         )
            #     )
            #     entry["reaction_description"] = reaction_text
            #     entry["reaction_formula"] = None
            #     entry["reactants"] = None
            #     entry["products"] = None
            # else:
            try:
                reactants, products = parse_reaction_string_metabolites(reaction_text)
            except ValueError:
                warnings.warn(
                    "Could not split entry ({}) as reaction: {}".format(
                        entry["ec_number"], reaction_text
                    )
                )
                entry["reaction_description"] = reaction_text
                entry["reaction_formula"] = None
                entry["reactants"] = None
                entry["products"] = None

            else:
                entry["reaction_description"] = None
                entry["reaction_formula"] = reaction_text
                entry["reactants"] = reactants
                entry["products"] = products
        entry["reversible"] = self.get_reversibility(entry["full_text"])
        return entry

    def get_wild_type(self, text):
        if text is None:
            return None
        elif WILD_TYPE_REGEX.search(text):
            return True
        else:
            return False

    def get_mutant(self, text):
        if text is None:
            return None
        elif MUTANT_REGEX.search(text):
            return True
        else:
            return False

    def _parse_AC(self, entry):
        entry["activator"] = entry["cleaned"]
        return entry

    def _parse_AP(self, entry):
        entry["application"] = entry["cleaned"]
        return entry

    def _parse_CF(self, entry):
        entry["cofactor"] = entry["cleaned"]
        return entry

    def _parse_CL(self, entry):
        assert not entry["cleaned"]
        entry["cloned"] = entry["comment"]
        del entry["comment"]
        return entry

    def _parse_CR(self, entry):
        assert not entry["cleaned"]
        entry["crystallization"] = entry["comment"]
        del entry["comment"]
        return entry

    def _parse_EN(self, entry):
        entry["engineering"] = entry["cleaned"]
        return entry

    def _parse_EXP(self, entry):
        entry["expression"] = entry["cleaned"]
        return entry

    def _parse_GI(self, entry):
        entry["general_information"] = entry["cleaned"]
        return entry

    def _parse_GS(self, entry):
        assert not entry["cleaned"]
        entry["general_stability"] = entry["comment"]
        del entry["comment"]
        return entry

    def _parse_IC50(self, entry):
        entry = self.get_single_or_range(entry["cleaned"], entry, "ic50")
        entry["temperature"], entry["ph"] = self.get_temp_ph(entry["comment"])
        entry["metabolite"] = (
            entry["specific_field"] if entry["specific_field"] else None
        )
        entry["wild_type"] = self.get_wild_type(entry["comment"])
        entry["mutant"] = self.get_mutant(entry["comment"])
        return entry

    def _parse_IN(self, entry):
        entry["inhibitor"] = entry["cleaned"]
        return entry

    def _parse_KI(self, entry):
        entry = self.get_single_or_range(entry["cleaned"], entry, "ki")
        entry["temperature"], entry["ph"] = self.get_temp_ph(entry["comment"])
        entry["metabolite"] = (
            entry["specific_field"] if entry["specific_field"] else None
        )
        entry["wild_type"] = self.get_wild_type(entry["comment"])
        entry["mutant"] = self.get_mutant(entry["comment"])
        return entry

    def _parse_KM(self, entry):
        entry = self.get_single_or_range(entry["cleaned"], entry, "km")
        entry["temperature"], entry["ph"] = self.get_temp_ph(entry["comment"])
        entry["metabolite"] = (
            entry["specific_field"] if entry["specific_field"] else None
        )
        entry["wild_type"] = self.get_wild_type(entry["comment"])
        entry["mutant"] = self.get_mutant(entry["comment"])
        return entry

    def _parse_KKM(self, entry):
        entry = self.get_single_or_range(entry["cleaned"], entry, "kkm")
        entry["temperature"], entry["ph"] = self.get_temp_ph(entry["comment"])
        entry["metabolite"] = (
            entry["specific_field"] if entry["specific_field"] else None
        )
        entry["wild_type"] = self.get_wild_type(entry["comment"])
        entry["mutant"] = self.get_mutant(entry["comment"])
        return entry

    def _parse_LO(self, entry):
        entry["localization"] = entry["cleaned"]
        return entry

    def _parse_ME(self, entry):
        entry["metal_ions"] = entry["cleaned"]
        return entry

    def _parse_MW(self, entry):
        entry = self.get_single_or_range(entry["cleaned"], entry, "molecular_weight")
        return entry

    def _parse_NSP(self, entry):
        entry = self.parse_reaction(entry)
        return entry

    def _parse_OS(self, entry):
        assert not entry["cleaned"]
        entry["oxidation_stability"] = entry["comment"]
        del entry["comment"]
        return entry

    def _parse_OSS(self, entry):
        entry["solvent"] = entry["cleaned"]
        return entry

    def _parse_PHO(self, entry):
        entry = self.get_single_or_range(entry["cleaned"], entry, "ph_optimum")
        return entry

    def _parse_PHR(self, entry):
        entry = self.get_single_or_range(entry["cleaned"], entry, "ph_range")
        return entry

    def _parse_PHS(self, entry):
        entry = self.get_single_or_range(entry["cleaned"], entry, "ph_stability")
        return entry

    def _parse_PI(self, entry):
        entry = self.get_single_or_range(entry["cleaned"], entry, "pi")
        return entry

    def _parse_PM(self, entry):
        entry["posttranslational_modification"] = entry["cleaned"]
        return entry

    def _parse_PR(self, entry):
        if len(entry["protein_ref"]) > 1:
            raise ValueError("Multiple protein references in one definition.")
        entry["protein_ref"] = entry["protein_ref"][0]
        entry["organism"] = entry["cleaned"]
        uniprot = UNIPROT_REGEX.search(entry["organism"])
        if uniprot is not None:
            entry["uniprot"] = uniprot.group(1)
            entry["organism"] = entry["organism"].replace(uniprot.group(0), "").strip()
        else:
            entry["uniprot"] = None
        swissprot = SWISSPROT_REGEX.search(entry["organism"])
        if swissprot is not None:
            entry["swissprot"] = swissprot.group(1)
            entry["organism"] = (
                entry["organism"].replace(swissprot.group(0), "").strip()
            )
        else:
            entry["swissprot"] = None
        if uniprot is None and swissprot is None:
            # Try the uniprot regex without the Uniprot marker.
            uniprot = SECONDARY_UNIPROT_REGEX.search(entry["organism"])
            if uniprot is not None:
                entry["uniprot"] = uniprot.group(0)
                entry["organism"] = (
                    entry["organism"].replace(uniprot.group(0), "").strip()
                )
        return entry

    def _parse_PU(self, entry):
        assert not entry["cleaned"]
        entry["purification"] = entry["comment"]
        del entry["comment"]
        return entry

    def _parse_RE(self, entry):
        entry = self.parse_reaction(entry)
        return entry

    def _parse_RF(self, entry):
        if len(entry["literature_ref"]) > 1:
            raise ValueError("Multiple protein references in one definition.")
        entry["literature_ref"] = entry["literature_ref"][0]
        # Reference has different format, so don't use clean.
        # In addition, it might have more text in parenthesis, so the comment is not correct.
        # for this field
        reference = entry["full_text"].replace(
            "{{{}}}".format(entry["specific_field"]), ""
        )
        entry["reference"] = LIT_REF_REGEX.sub("", reference).strip()
        entry["pubmed_id"] = self.get_pubmed_id(entry["specific_field"])
        entry["comment"] = None
        return entry

    def _parse_REN(self, entry):
        assert not entry["cleaned"]
        entry["renatured"] = entry["comment"]
        del entry["comment"]
        return entry

    def _parse_RN(self, entry):
        entry["recommended_name"] = entry["full_text"].strip()
        del entry["comment"]
        return entry

    def _parse_RT(self, entry):
        entry["reaction_type"] = entry["full_text"].strip()
        del entry["comment"]
        return entry

    def _parse_SA(self, entry):
        entry = self.get_single_or_range(entry["cleaned"], entry, "specific_activity")
        entry["temperature"], entry["ph"] = self.get_temp_ph(entry["comment"])
        entry["metabolite"] = (
            entry["specific_field"] if entry["specific_field"] else None
        )
        entry["wild_type"] = self.get_wild_type(entry["comment"])
        entry["mutant"] = self.get_mutant(entry["comment"])
        return entry

    def _parse_SN(self, entry):
        entry["systematic_name"] = entry["full_text"].strip()
        del entry["comment"]
        return entry

    def _parse_SP(self, entry):
        entry = self.parse_reaction(entry)
        return entry

    def _parse_SS(self, entry):
        assert not entry["cleaned"]
        entry["storage_stability"] = entry["comment"]
        del entry["comment"]
        return entry

    def _parse_ST(self, entry):
        entry["source_tissue"] = entry["cleaned"]
        return entry

    def _parse_SU(self, entry):
        entry["subunits"] = entry["cleaned"]
        return entry

    def _parse_SY(self, entry):
        entry["synonym"] = entry["cleaned"]
        return entry

    def _parse_TN(self, entry):
        entry = self.get_single_or_range(entry["cleaned"], entry, "turnover_number")
        entry["temperature"], entry["ph"] = self.get_temp_ph(entry["comment"])
        entry["metabolite"] = (
            entry["specific_field"] if entry["specific_field"] else None
        )
        entry["wild_type"] = self.get_wild_type(entry["comment"])
        entry["mutant"] = self.get_mutant(entry["comment"])
        return entry

    def _parse_TO(self, entry):
        entry = self.get_single_or_range(entry["cleaned"], entry, "temperature_optimum")
        return entry

    def _parse_TR(self, entry):
        entry = self.get_single_or_range(entry["cleaned"], entry, "temperature_range")
        return entry

    def _parse_TS(self, entry):
        entry = self.get_single_or_range(
            entry["cleaned"], entry, "temperature_stability"
        )
        return entry


def parse_section(parser, lines, ec_number, strict=False):
    # Merge lines that are not full entries.
    entry = []
    for line in lines:
        match = SHORT_NAME_REGEX.match(line)
        # New entry, yield and clear entry.
        if match is not None and entry:
            try:
                yield parser.parse((" ".join(entry)), ec_number)
            except EmptyEntryException:
                pass
            except Exception as ex:
                if not strict:
                    with open("failures.txt", "a") as f:
                        f.write("{}\n".format(repr(ex)))
                        f.write("ID\t{}\n".format(ec_number))
                        f.writelines(entry)
                        f.write("\n")
                        # Don't forget to reset the entry if we just failed to parse!
                        entry = []
                    continue
                else:
                    raise
            entry = [line]
        # No new entry, so append only.
        else:
            entry.append(line)
    try:
        yield parser.parse((" ".join(entry)), ec_number)
    except EmptyEntryException:
        pass
    except Exception as ex:
        if not strict:
            with open("failures.txt", "a") as f:
                f.write("{}\n".format(repr(ex)))
                f.write("ID\t{}\n".format(ec_number))
                f.writelines(entry)
                f.write("\n")
                # Don't forget to reset the entry if we just failed to parse!
                entry = []
        else:
            raise


def parse_ec_block(parser, lines, ec_number):
    section_lines = []
    section_type = None
    for line in lines:
        match = FULL_NAME_REGEX.match(line)
        if match is not None:
            if section_type is not None:
                yield section_type, parse_section(parser, section_lines, ec_number)
            section_type = match.group(0)
            section_lines = []
        else:
            section_lines.append(line)
    yield section_type, parse_section(parser, section_lines, ec_number)


def parse_brenda(brenda_file_path, debug=False):
    parser = Parser(debug=debug)
    with open(brenda_file_path, "r") as brenda_file:
        ec_block = []
        ec_number = None
        for i, line in enumerate(brenda_file):
            # Skip copyright block.
            if line.startswith("*"):
                continue
            line = line.strip()
            # Skip lines empty after stripping white spaces.
            if not line:
                continue
            # Save EC once we find it
            elif line.startswith("ID\t"):
                if ec_number is not None:
                    warnings.warn("Double EC-number entry at line: {}".format(i))
                match = EC_REGEX.search(line)
                if match is None:
                    warnings.warn(
                        "Found ID entry with no regex match: {} - {}".format(i, line)
                    )
                ec_number = match.group()
            # Once we find the closing sequence, we are done with a block
            elif line.startswith("///"):
                yield ec_number, parse_ec_block(parser, ec_block, ec_number)
                ec_block = []
                ec_number = None
            else:
                ec_block.append(line)
    if ec_number is not None or ec_block:
        warnings.warn("Unfinished block: {}\n{}".format(ec_number, "\n".join(ec_block)))


def get_current_tables(conn):
    return {
        i[0]
        for i in conn.execute(
            """SELECT name
               FROM sqlite_master
               WHERE type='table'
               """
        )
    }


def update_sql_tables(conn, block, debug=False):
    c = conn.cursor()

    # Get current tables
    current_tables = get_current_tables(c)

    if 'literature' not in current_tables:
        # Create literature reference table
        c.execute(
            """CREATE TABLE literature (
                ec text,
                id integer,
                reference text,
                pubmed text,
                comment text
                )
            """
        )
        c.execute("""CREATE INDEX ec_ref_id_index ON literature(ec, id)""")

    if 'protein' not in current_tables:
        # Create protein reference table
        c.execute(
            """CREATE TABLE protein (
                ec text,
                id integer,
                organism text,
                pubmed text,
                uniprot text,
                swissprot text,
                comment text
                )
            """
        )
        c.execute("""CREATE INDEX ec_prot_id_index ON protein(ec, id)""")

    # Create protein to literature reference table.
    if 'protein_ref' not in current_tables:
        c.execute(
            """CREATE TABLE protein_ref (
                ec text,
                protein_id integer,
                ref_id integer
                )
            """
        )

    skip = {
        "full_text",
        "short_name",
        "specific_field",
        "literature_ref",
        "protein_ref",
        "products",
        "reactants",
        "cleaned",
    }
    if debug:
        skip.remove("full_text")
        skip.remove("cleaned")
    real = {
        "temperature",
        "temperature_stability",
        "temperature_range",
        "temperature_optimum",
        "ph",
        "ph_stability",
        "ph_optimum",
        "ph_range",
        "ki",
        "km",
        "kkm",
        "specific_activity",
        "turnover_number",
        "pi",
        "IC50",
        "molecular_weight",
    }
    real.update({i + "_min" for i in real}, {i + "_max" for i in real})
    integer = {"wild_type", "mutant", "reversible"}

    template = """CREATE TABLE {table_name} (
                    entry integer,
                    {columns}
                    )
               """

    template_ref = """CREATE TABLE {table_name}_ref (
                        ec text,
                        entry integer,
                        ref_id integer
                        )
                   """

    template_prot = """CREATE TABLE {table_name}_prot (
                        ec text,
                        entry integer,
                        prot_id integer
                        )
                    """

    tables = []
    for table in block.keys():
        if table in ("PROTEIN", "REFERENCE"):
            continue
        else:
            table_name = table.lower()
            fields = []
            for k, v in block[table][0].items():
                if k in skip:
                    continue
                elif isinstance(v, list):
                    continue
                elif k in real:
                    v_t = "real"
                elif k in integer:
                    v_t = "integer"
                else:
                    v_t = "text"
                fields.append((k, v_t))
            tables.append((table_name, fields))
            if table_name not in current_tables:
                columns = ",\n".join([" ".join(i) for i in fields])
                c.execute(template.format(columns=columns, table_name=table_name))
                c.execute(template_ref.format(table_name=table_name))
                c.execute(template_prot.format(table_name=table_name))
    return tables


def read_in_sql_data(conn, block, tables):
    c = conn.cursor()
    # Load references, and protein references first.
    if "REFERENCE" in block:
        c.executemany(
            """INSERT INTO literature (
                ec, id, reference, pubmed, comment)
                values (
                :ec_number, :literature_ref, :reference, :pubmed_id, :comment)
            """,
            block["REFERENCE"],
        )

    if "PROTEIN" in block:
        c.executemany(
            """INSERT INTO protein (
                ec, id, organism, uniprot, swissprot, comment)
                values (
                :ec_number, :protein_ref, :organism,
                :uniprot, :swissprot, :comment)
            """,
            block["PROTEIN"],
        )

        # Next we load the protein reference to literature reference data.
        entries = [
            (i["ec_number"], i["protein_ref"], j)
            for i in block["PROTEIN"]
            for j in i["literature_ref"]
        ]
        c.executemany(
            """INSERT INTO protein_ref (
                ec, protein_id, ref_id)
                values (?, ?, ?)
            """,
            entries,
        )

    template = """INSERT INTO {table_name} (
                    entry, {field_names})
                    values (:entry, {values})
               """

    template_ref = """INSERT INTO {table_name}_ref (
                        ec, entry, ref_id)
                        values (?, ?, ?)
                   """

    template_prot = """INSERT INTO {table_name}_prot (
                          ec, entry, prot_id)
                          values (?, ?, ?)
                    """

    # Finally, for each category, load the data first, then load the
    # references and protein references afterwards.
    for table, fields in tables:
        try:
            data = block[table.upper()]
        except KeyError:
            # No data for this category in this ec number
            continue
        [i.update({"entry": idx}) for idx, i in enumerate(data)]
        field_names = ", ".join([i[0] for i in fields])
        value_placeholders = ", ".join(":" + i[0] for i in fields)
        t = template.format(
            table_name=table, field_names=field_names, values=value_placeholders
        )
        c.executemany(t, data)

        ref_entries = [
            (i["ec_number"], idx, j)
            for idx, i in enumerate(data)
            for j in i["literature_ref"]
        ]
        c.executemany(template_ref.format(table_name=table), ref_entries)

        prot_entries = [
            (i["ec_number"], idx, j)
            for idx, i in enumerate(data)
            for j in i["protein_ref"]
        ]
        c.executemany(template_prot.format(table_name=table), prot_entries)


def add_metabolites_and_reaction_tables(conn):
    # Make sure to check if each of these tables actually exist, as some entries do not have all categories.
    available_tables_query = "SELECT name FROM sqlite_master WHERE type = 'table'"
    all_tables = [result[0] for result in conn.execute(available_tables_query)]

    # SQL: Find all reactions
    reaction_tables = []
    template = "SELECT entry, ec_number, reaction_formula, reaction_description, '{table}' as reaction_source FROM {table}"
    for table in ("reaction", "substrate_product", "natural_substrate_product"):
        if table in all_tables:
            reaction_tables.append(template.format(table=table))
    all_reactions = "\nUNION ALL\n".join(reaction_tables)

    reactions = {}
    reaction_entry_xref = []
    metabolites = {}
    metabolite_reaction_xref = []

    current_rid = 0
    current_mid = 0
    for entry, ec, reaction_formula, reaction_description, table in conn.execute(
        all_reactions
    ):
        combined_reaction = (reaction_formula, reaction_description)
        # New reaction entries
        if combined_reaction not in reactions:
            rid = current_rid
            current_rid += 1

            reactions[combined_reaction] = rid

            # Check metabolites
            if reaction_formula:
                try:
                    substrates, products = parse_reaction_string_metabolites(
                        reaction_formula
                    )
                except ValueError:
                    pass
                else:
                    # Add metabolite if not assigned yet.
                    for _, m in substrates + products:
                        if m not in metabolites:
                            metabolites[m] = current_mid
                            current_mid += 1

                    # Add xref
                    for s, m in [(-s, m) for s, m in substrates] + products:
                        mid = metabolites[m]
                        metabolite_reaction_xref.append((rid, mid, s))

        # Known reaction entries
        else:
            rid = reactions[combined_reaction]

        reaction_entry_xref.append((rid, ec, table, entry))

    # Add tables
    # Create reaction reference table.
    # This is used for the entries: reaction, substrate product and natural substrate product.
    conn.execute(
        """CREATE TABLE reactions (
            id integer UNIQUE,
            reaction_formula text,
            reaction_description text
            )
        """
    )
    conn.execute(
        """CREATE TABLE metabolites (
            id integer UNIQUE,
            metabolite text UNIQUE
            )
        """
    )
    conn.execute(
        """CREATE TABLE reaction_reference (
            reac_id integer,
            category text,
            ec text,
            entry integer,
            FOREIGN KEY(reac_id) REFERENCES reactions(id)
            )
        """
    )
    conn.execute(
        """CREATE TABLE reaction_metabolites (
            reac_id integer,
            met_id integer,
            stoichiometry,
            FOREIGN KEY(reac_id) REFERENCES reactions(id),
            FOREIGN KEY(met_id) REFERENCES metabolites(id)
            )
        """
    )

    # Read in data
    conn.executemany(
        """INSERT INTO reactions (
            id, reaction_formula, reaction_description)
            values (?, ?, ?)
        """,
        [(v,) + k for k, v in reactions.items()],
    )

    conn.executemany(
        """INSERT INTO metabolites (
            id, metabolite)
            values (?, ?)
        """,
        [(v, k) for k, v in metabolites.items()],
    )

    conn.executemany(
        """INSERT INTO reaction_reference (
            reac_id, ec, category, entry)
            values (?, ?, ?, ?)
        """,
        reaction_entry_xref,
    )

    conn.executemany(
        """INSERT INTO reaction_metabolites (
            reac_id, met_id, stoichiometry)
            values (?, ?, ?)
        """,
        metabolite_reaction_xref,
    )


# Notes:
# Some reactions are saved as actual reaction formulas, some as descriptions of the mechanism
# (especially more general enzymes such as peptidases). A few reactions still fail to parse
# and will give a warning. Note that other failures to parse will be written to
# 'failures.txt' in the current directory. Currently no entries (except for reactions) fail
# to parse, although more might be parsed incorrectly...
# Finally, note that each entry might have saved extra information under the name "more"
# This generally contains more references and notes where the value could not be used
# in the BRENDA format, but that might be relevant nonetheless.
# This does makes it so there are entries dotted around each table with "more", "more = ?"
# or -999.0 in various fields.


if __name__ == "__main__":
    test = False
    add_metabolites_reactions_xref = True

    # Actually load all the data
    if not test:
        # You can change the input and output path here if needed.
        brenda_file_path = "brenda_download.txt"
        database_path = "brenda.db"

        if os.path.exists(database_path):
            os.remove(database_path)
        conn = sqlite3.connect(database_path)

        brenda_iterator = parse_brenda(brenda_file_path, debug=False)

        # Chunk it per EC block.
        for ec_number, brenda_block in brenda_iterator:
            block = collections.defaultdict(list)
            for section_type, section in brenda_block:
                for entry in section:
                    block[section_type].append(entry)
            block = dict(block)

            with conn:
                tables = update_sql_tables(conn, block)

            with conn:
                read_in_sql_data(conn, block, tables)

    # Test version where we load just one ec-number for quick testing.
    else:
        parse_ec = "1.1.1.1"
        brenda_file_path = "brenda_download.txt"
        brenda = parse_brenda(brenda_file_path, debug=True)

        # Thanks to the lazy parsing, it is quite quick to just search for the ec wanted.
        for ec_number, brenda_block in brenda:
            if ec_number != parse_ec:
                pass
            else:
                break

        block = collections.defaultdict(list)
        for section_type, section in brenda_block:
            for entry in section:
                block[section_type].append(entry)
        block = dict(block)

        # Test to save as json
        with open("test.json", "w") as outfile:
            json.dump(block, outfile, indent=2)

        # Test to save as sql
        if os.path.exists("test.db"):
            os.remove("test.db")
        conn = sqlite3.connect("test.db")

        with conn:
            tables = update_sql_tables(conn, block)

        with conn:
            read_in_sql_data(conn, block, tables)

    if add_metabolites_reactions_xref:
        with conn:
            add_metabolites_and_reaction_tables(conn)
