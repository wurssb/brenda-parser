#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Example queries to retrieve entries based on one or more metabolites.

Author: Rik van Rosmalen (rikpetervanrosmalen@gmail.com)
"""
import sqlite3
import pathlib

import pandas as pd


def get_related_reactions_by_metabolites(conn, metabolites):
    template = """
    SELECT
        reaction_reference.ec,
        reaction_reference.category,
        reaction_reference.entry as entry_id,
        metabolites.id as met_id,
        metabolites.metabolite,
        reactions.id as reac_id,
        reactions.reaction_formula,
        reactions.reaction_description
    FROM metabolites
    INNER JOIN reaction_reference
    ON (
        reaction_reference.reac_id = reactions.id
    )
    INNER JOIN reactions
    ON (
        reaction_metabolites.reac_id = reactions.id
    )
    INNER JOIN reaction_metabolites
    ON (
        metabolites.id = reaction_metabolites.met_id
    )
    WHERE (
        metabolites.metabolite in ({metabolites})
    )
    ORDER BY reaction_reference.ec, metabolites.metabolite ASC
    """
    return pd.read_sql_query(
        template.format(metabolites=", ".join(repr(i) for i in metabolites)), conn
    )


def get_regulator_entries_by_metabolites(conn, metabolites):
    template = """
    SELECT
        activating_compound.ec_number,
        activating_compound.entry,
        activating_compound.activator as metabolite,
        'activator' as role,
        activating_compound.comment
    FROM activating_compound
    WHERE activating_compound.activator in ({metabolites})
    UNION
    SELECT
        inhibitors.ec_number,
        inhibitors.entry,
        inhibitors.inhibitor as metabolite,
        'inhibitor' as role,
        inhibitors.comment
    FROM inhibitors
    WHERE inhibitors.inhibitor in ({metabolites})
    UNION
    SELECT
        cofactor.ec_number,
        cofactor.entry,
        cofactor.cofactor as metabolite,
        'cofactor' as role,
        cofactor.comment
    FROM cofactor
    WHERE cofactor.cofactor in ({metabolites})
    UNION
    SELECT
        metals_ions.ec_number,
        metals_ions.entry,
        metals_ions.metal_ions as metabolite,
        'metal_ion' as role,
        metals_ions.comment
    FROM metals_ions
    WHERE metals_ions.metal_ions in ({metabolites})
    ORDER BY ec_number, metabolite, role ASC
    """
    return pd.read_sql_query(
        template.format(metabolites=", ".join(repr(i) for i in metabolites)), conn
    )


def get_parameter_entries_by_metabolites(conn, metabolites):
    template = """
    SELECT
        ic50_value.ec_number,
        ic50_value.entry,
        ic50_value.metabolite as metabolite,
        'ic_50' as role,
        ic50_value.ic50 as value,
        ic50_value.ic50_min as value_min,
        ic50_value.ic50_max as value_max,
        ic50_value.temperature as temperature,
        ic50_value.ph as ph,
        ic50_value.wild_type as wild_type,
        ic50_value.mutant as mutant,
        ic50_value.comment
    FROM ic50_value
    WHERE ic50_value.metabolite in ({metabolites})
    UNION
    SELECT
        ki_value.ec_number,
        ki_value.entry,
        ki_value.metabolite as metabolite,
        'ki' as role,
        ki_value.ki as value,
        ki_value.ki_min as value_min,
        ki_value.ki_max as value_max,
        ki_value.temperature as temperature,
        ki_value.ph as ph,
        ki_value.wild_type as wild_type,
        ki_value.mutant as mutant,
        ki_value.comment
    FROM ki_value
    WHERE ki_value.metabolite in ({metabolites})
    UNION
    SELECT
        km_value.ec_number,
        km_value.entry,
        km_value.metabolite as metabolite,
        'km' as role,
        km_value.km as value,
        km_value.km_min as value_min,
        km_value.km_max as value_max,
        km_value.temperature as temperature,
        km_value.ph as ph,
        km_value.wild_type as wild_type,
        km_value.mutant as mutant,
        km_value.comment
    FROM km_value
    WHERE km_value.metabolite in ({metabolites})
    UNION
    SELECT
        turnover_number.ec_number,
        turnover_number.entry,
        turnover_number.metabolite as metabolite,
        'turnover_number' as role,
        turnover_number.turnover_number as value,
        turnover_number.turnover_number_min as value_min,
        turnover_number.turnover_number_max as value_max,
        turnover_number.temperature as temperature,
        turnover_number.ph as ph,
        turnover_number.wild_type as wild_type,
        turnover_number.mutant as mutant,
        turnover_number.comment
    FROM turnover_number
    WHERE turnover_number.metabolite in ({metabolites})
    ORDER BY ec_number, metabolite, role ASC
    """
    return pd.read_sql_query(
        template.format(metabolites=", ".join(repr(i) for i in metabolites)), conn
    )


if __name__ == "__main__":
    database_path = pathlib.Path("brenda.db").resolve()
    output_path = pathlib.Path("brenda_metabolites_query.xlsx").resolve()

    # List the metabolites we're interested in by name.
    # Brenda is not always consistent with naming, so include synonyms as required.
    metabolites = [
        "curcumin",
        "demethoxycurcumin",
    ]

    assert database_path.exists()

    with sqlite3.connect(database_path) as conn, pd.ExcelWriter(output_path) as outfile:
        get_related_reactions_by_metabolites(conn, metabolites).to_excel(
            outfile, sheet_name="reactions", engine="xlsxwriter"
        )
        get_regulator_entries_by_metabolites(conn, metabolites).to_excel(
            outfile, sheet_name="regulators", engine="xlsxwriter"
        )
        get_parameter_entries_by_metabolites(conn, metabolites).to_excel(
            outfile, sheet_name="parameters", engine="xlsxwriter"
        )
