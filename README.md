# Brenda-parser
Parses the Brenda flat text file download and outputs an SQLite database or JSON file.

For the SQLite database, the layout is the following. Each of the Brenda fields is extracted into 3 separate tables.
 1. The actual information about the field (e.g. km, kcat, cofactor, metal-ion etc.). This uses the same naming as the Brenda website.
 Always includes the ec-number *(ec)* and an entry identification number *(entry)*. The **combination** of these two is unique for each Brenda Field.
 2. The literature cross-references, noted as *{brenda_field}_ref*.
 This table cross-references the *(ec, entry)* with the literature reference entry *(ec, ref_id)*.
 3. The protein cross-references, noted as *{brenda_field}_prot*.
 This table cross-references the *(ec, entry)* with the protein entry *(ec, prot_id)*.
 **Note:** Each entry can have multiple literature and protein references, and each literature or protein reference can refer to multiple entries!

Finally, there are seven more global tables:
 1. The *literature* table. This has all the literature references as full-text and optional *pubmed* identifier, uniquely identified by the combination *(ec, id)*.
 2. The *protein* table. This has all the protein references, uniquely identified by the combination *(ec, id)*. This table contains the *organism* for all proteins and an optional *uniprot* or *swissprot* identifier.
 3. The *protein_ref* table. This contains literature references *(ec, ref_id)* from protein references *(ec, prot_id)*.
 4. The *reactions* table. This table contains all *unique* reactions and their reaction id.
 5. The *reaction_reference* table. This table cross-references the reactions table with the relevant Brenda fields *(reaction_id, ec, category, entry)*.
 6. The *metabolites* table. This table contains all *unique* metabolites and their metabolite id.
 7. The *reaction_metabolites* table. This table contains stoichiometry information as *(reaction_id, metabolite_id, stoichiometry)*.

**Note:** All tables have a *comment* field, containing information that was not in the fixed fields.

**Final note:** If an entry refuses to parse it will be saved to *failures.txt*. If a reaction entry parses, but the reaction equation fails to parse a warning will be printed.

Two examples with SQL queries based on the exported databases are given:
 * `metabolite_query.py` - Shows how to retrieve reactions, regulation and parameter values based on the name of one or more metabolites.
 * `species_query.py` - Shows how to retrieve all entries for every category referencing a certain species.

 **Note:** The examples require *pandas* and *xlsxwriter* to be installed to generate the xlsx output files.

## Requirements:
- Python 3.4+ (Possibly older Python 3 versions, but these remain untested.)
- The Brenda database in text file format.
The Brenda data file can be obtained from [their website](https://www.brenda-enzymes.org/download_brenda_without_registration.php). Please note that you'll have to accept the license terms.

## Usage:
1. Clone or download this repository.
2. Download the Brenda text file and save in this directory as `brenda_download.txt`.
3. Run `python brenda.py`.
4. The output will be in `brenda.db`.
5. Any entries that failed to parse will be in `failures.txt`.
6. `brenda.db` can be read using any tool or library in your favourite language for [sqllite](https://www.sqlite.org/docs.html).
See [sqllite browser](https://sqlitebrowser.org/) for a tool with a graphical interface.

**Note:** Paths can be modified in the script.

## Implementation:
- The parser uses a combination of regular expressions and a simple stack based parser to deal with ambiguity of nested structures within chemical compound names and optional comments or references.
- The parser uses lazy evaluation of each Brenda EC entry block and sub-block. This allows you to quickly skip to a specific E.C. number or filter on a specific property. However, it might be easier to just generate the SQL database and query on that instead.
- For further detail see `brenda.py` and the README file included with the Brenda download.

## Authors:
- Rik van Rosmalen

## License:
This project is licensed under the MIT License - see the LICENSE file for details.

## BRENDA
BRENDA is available at [www.brenda-enzymes.org](https://www.brenda-enzymes.org).

### For references on BRENDA see:
- Sandra Placzek, Ida Schomburg, Antje Chang, Lisa Jeske, Marcus Ulbrich, Jana Tillack, Dietmar Schomburg; BRENDA in 2017: new perspectives and new tools in BRENDA, _Nucleic Acids Research_, Volume 45, Issue D1, 4 January 2017, Pages D380–D388, [https://doi.org/10.1093/nar/gkw952](https://doi.org/10.1093/nar/gkw952)
- [More references](https://www.brenda-enzymes.org/introduction.php#references)
